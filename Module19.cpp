﻿#include <iostream>

using namespace std;

class Animal
{
public:
	Animal() {}
	virtual string Voice()
	{
		cout << "..." << endl;
		return "...";
	}
};

class Dog : public Animal
{
public:
	Dog() {}
	string Voice() override
	{
		cout << "Woof!" << endl;
		return "Woof!";
	}
};

class Cat : public Animal
{
public:
	Cat() {}
	string Voice() override
	{
		cout << "Mrrrrrr" << endl;
		return "Mrrrrrr";
	}
};

class Snake : public Animal
{
public:
	Snake() {}
	string Voice() override
	{
		cout << "Ssssss" << endl;
		return "Ssssss";
	}
};

int main()
{
	int amount = 5;
	Animal* dog = new Dog();
	Animal* cat = new Cat();
	Animal* snake = new Snake();
	Animal** animals = new Animal * [amount] {dog, cat, snake, dog, cat};

	for (int i = 0; i < amount; ++i)
	{
		animals[i]->Voice();
	}
}
